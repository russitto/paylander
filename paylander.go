package main

import (
    "log"
    "time"
    "bytes"
    "net/smtp"
    "text/template"
    "golang.org/x/crypto/bcrypt"
    "github.com/gin-gonic/gin"
    "github.com/gin-gonic/gin/binding"
    "github.com/gin-gonic/contrib/sessions"
    "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"
    "./models"
)

// @TODO pasar las funciones auxiliares a un ./utils

func genPass() string {
    ti := time.Now().Format(time.StampNano)
    hash, err := bcrypt.GenerateFromPassword([]byte(ti), 1)
    if err != nil {
        panic(err)
    }
    sHash := string(hash)
    length := len(sHash)
    return sHash[length - 10:length]
}

func hashPass(pass string) string {
    hash, err := bcrypt.GenerateFromPassword([]byte(pass), 1)
    if err != nil {
        panic(err)
    }
    return string(hash)
}

func sendMail(to []string, doc bytes.Buffer) bool {
    var err error
    emailUser := &models.EmailUser{"matias@qubit.tv", "", "smtp.gmail.com", "587"}
    auth := smtp.PlainAuth("",
        emailUser.Username,
        emailUser.Password,
        emailUser.Server)
    err = smtp.SendMail(emailUser.Server+":"+emailUser.Port,
        auth,
        emailUser.Username,
        to,
        doc.Bytes())
    if err != nil {
        log.Println("ERROR: attempting to send a mail ", err)
        return false
    } else {
        return true
    }
}

func registerEmail(user string, pass string) bool {
    var err error
    var doc bytes.Buffer
    context := &models.SmtpTemplateData{
        "Paylander",
        user,
        "Welcome!",
        "Welcome to Paylander\nYour temporary password is: " + pass,
        "Greetings"}

    t := template.New("EmailTemplate")
    t, err = t.Parse(models.EmailTemplate)
    if err != nil {
        log.Println("error trying to parse mail template")
    }
    err = t.Execute(&doc, context)
    if err != nil {
        log.Println("error trying to execute mail template")
    }
    return sendMail([]string{user}, doc)
}

func main() {
    r := gin.Default()
    r.Use(gin.Logger())

    mgosess, err := mgo.Dial("localhost")
    if err != nil {
        panic(err)
    }
    defer mgosess.Close()
    mgosess.SetMode(mgo.Monotonic, true)

    store := sessions.NewCookieStore([]byte("larechuchaweon"))
    r.Use(sessions.Sessions("paylander", store))

	r.LoadHTMLGlob("templates/*")
    r.Static("/assets", "./assets")

    r.GET("/", func(c *gin.Context) {
        session := sessions.Default(c)
        var user string

        v := session.Get("user")
        if v == nil {
            user = ""
        } else {
            user = v.(string)
        }

        messDefault := session.Flashes()
        messError := session.Flashes("error")
        session.Save()
        c.HTML(200, "index.html", gin.H{"user": user, "messDefault": messDefault, "messError": messError})
    })

    r.POST("/register", func(c *gin.Context) {
        session := sessions.Default(c)

        var form models.RegisterForm
        c.BindWith(&form, binding.MultipartForm)

        session.Set("user", form.User)

        m := mgosess.DB("paylander").C("users")
        result := models.LoginForm{}
        err = m.Find(bson.M{"user": form.User}).One(&result)
        if err != nil && err.Error() == "not found" {
            passwd := genPass()
            // @TODO mandar email con user y pass
            log.Println("La pass chabón!: " + passwd)
            hashed := hashPass(passwd)
            err = m.Insert(&models.LoginForm{form.User, hashed})
            session.AddFlash("Created")
        } else {
            session.AddFlash("Existent found", "error")
        }
        session.Save()
        c.Redirect(301, "/")
    })

    r.POST("/login", func(c *gin.Context) {
        session := sessions.Default(c)

        var form models.LoginForm
        c.BindWith(&form, binding.MultipartForm)

        session.Set("user", form.User)

        m := mgosess.DB("paylander").C("users")
        result := models.LoginForm{}
        err = m.Find(bson.M{"user": form.User}).One(&result)

        if err != nil && err.Error() == "not found" {
            session.AddFlash("Error at user or pass", "error")
        } else {
            compare := bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(form.Password))
            log.Println(hashPass(form.Password))
            if compare == nil {
                session.AddFlash("Logged!!!")
            } else {
                session.AddFlash("Error at user or pass", "error")
            }
        }
        session.Save()
        c.Redirect(301, "/")
    })

    r.Run(":8080")
}
