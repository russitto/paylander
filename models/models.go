package models

type LoginForm struct {
    User     string `form:"user" binding:"required"`
    Password string `form:"password" binding:"required"`
}

type RegisterForm struct {
    User     string `form:"user" binding:"required"`
}

type EmailUser struct {
    Username string
    Password string
    Server   string
    Port     string
}

type SmtpTemplateData struct {
    From    string
    To      string
    Subject string
    Body    string
    Sign    string
}

const EmailTemplate = `From: {{.From}}
To: {{.To}}
Subject: {{.Subject}}

{{.Body}}
--
{{.Sign}}
`
